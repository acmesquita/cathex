module ConsumosHelper

    def veiculos_disponiveis
        if current_user.kind == 1
            return Veiculo.do_funcionario(current_user.id)
        else 
            return Veiculo.all
        end
    end
end
