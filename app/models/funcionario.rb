# == Schema Information
#
# Table name: funcionarios
#
#  id         :integer          not null, primary key
#  nome       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#  veiculo_id :integer
#
# Indexes
#
#  index_funcionarios_on_user_id     (user_id)
#  index_funcionarios_on_veiculo_id  (veiculo_id)
#

class Funcionario < ApplicationRecord
  belongs_to :veiculo
  belongs_to :user

  scope :do_user, -> (user) { where(:user => user)}
  scope :do_veiculo, -> (veiculo_id) { where("veiculo_id = ?", veiculo_id)}

end
