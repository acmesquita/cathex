# == Schema Information
#
# Table name: consumos
#
#  id                 :integer          not null, primary key
#  data_abastecimento :datetime
#  quantidade_litros  :float
#  valor              :float
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  veiculo_id         :integer
#
# Indexes
#
#  index_consumos_on_veiculo_id  (veiculo_id)
#

class Consumo < ApplicationRecord
  belongs_to :veiculo
  
  scope :do_veiculo, -> (veiculo) { where(:veiculo => veiculo)}
  scope :do_funcionario, ->(user_id) { Consumo.do_veiculo(Funcionario.do_user(user_id).first.veiculo.id) }

  scope :is_funcionario, ->(user_id, consumo_id) { Consumo.do_funcionario(user_id).where(:id => consumo_id)}

  scope :do_rank, -> (competencia){
    connection.select_all "select f.nome,
            sum(c.valor) as valor_total,
            sum(c.quantidade_litros) as litros_total
    from
        funcionarios as f
            inner join veiculos as v on f.veiculo_id = v.id
            inner join consumos as c on c.veiculo_id = v.id
    where
        c.data_abastecimento between '#{competencia.strftime("%Y-%m-01")}' and '#{competencia}'
    group by f.nome
    order by valor_total, litros_total desc"
  }

  validates_presence_of :valor, :quantidade_litros, on: [:create, :update], message: "não podem ficar me branco."

  def to_s
    "R$ #{self.valor} - #{self.data_abastecimento.strftime("%d/%m/%Y")}"
  end

end
