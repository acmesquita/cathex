# == Schema Information
#
# Table name: veiculos
#
#  id         :integer          not null, primary key
#  ano        :integer
#  modelo     :string
#  placa      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Veiculo < ApplicationRecord

    has_many :consumos

    scope :do_funcionario, -> (user_id) { where("id = ?", Funcionario.do_user(user_id).first.veiculo.id)}

    def to_s
      "#{self.placa}/#{self.ano}"
    end

    def consumo_medio
      total = self.total()
      count = self.consumos.count == 0 ? 1 : self.consumos.count 
      "R$ #{total/count}"
    end

    def consumo_total
      "R$ #{ self.total }"
    end

    def total
      self.consumos.map{|c| c.valor}.sum
    end

end
