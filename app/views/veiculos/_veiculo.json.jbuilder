json.extract! veiculo, :id, :modelo, :placa, :ano, :created_at, :updated_at
json.url veiculo_url(veiculo, format: :json)
