json.extract! consumo, :id, :data_abastecimento, :valor, :quantidade_litros, :veiculo_id, :created_at, :updated_at
json.url consumo_url(consumo, format: :json)
