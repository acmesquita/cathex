class DashboardController < ApplicationController
  def index
    @consumos_rank = Consumo.do_rank(Date.today+1)
  end
end
