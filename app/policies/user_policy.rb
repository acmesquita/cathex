class UserPolicy < ApplicationPolicy

  def logado?
    !user.nil?
  end
  
  def veiculo?
    (user.kind == 0)
  end

  def funcionario?
    (user.kind == 0)
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
