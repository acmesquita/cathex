class ConsumoPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      if user.kind == 0
        scope.all.order(data_abastecimento: :desc)
      else
        scope.do_funcionario(user.id).order(data_abastecimento: :desc)
      end
    end
  end
end
