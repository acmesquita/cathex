Rails.application.routes.draw do
  devise_for :users
  get 'dashboard/index'
  resources :consumos
  resources :funcionarios
  resources :veiculos
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "dashboard#index"

  match '*path' => redirect('/'), via: :get
end
