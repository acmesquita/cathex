class CreateFuncionarios < ActiveRecord::Migration[5.2]
  def change
    create_table :funcionarios do |t|
      t.references :veiculo, foreign_key: true
      t.string :nome

      t.timestamps
    end
  end
end
