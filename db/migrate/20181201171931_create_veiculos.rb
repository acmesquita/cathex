class CreateVeiculos < ActiveRecord::Migration[5.2]
  def change
    create_table :veiculos do |t|
      t.string :modelo
      t.string :placa
      t.integer :ano

      t.timestamps
    end
  end
end
