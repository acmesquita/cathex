class CreateConsumos < ActiveRecord::Migration[5.2]
  def change
    create_table :consumos do |t|
      t.datetime :data_abastecimento
      t.float :valor
      t.float :quantidade_litros
      t.references :veiculo, foreign_key: true

      t.timestamps
    end
  end
end
