require "application_system_test_case"

class ConsumosTest < ApplicationSystemTestCase
  setup do
    @consumo = consumos(:one)
  end

  test "visiting the index" do
    visit consumos_url
    assert_selector "h1", text: "Consumos"
  end

  test "creating a Consumo" do
    visit consumos_url
    click_on "New Consumo"

    fill_in "Data Abastecimento", with: @consumo.data_abastecimento
    fill_in "Quantidade Litros", with: @consumo.quantidade_litros
    fill_in "Valor", with: @consumo.valor
    fill_in "Veiculo", with: @consumo.veiculo_id
    click_on "Create Consumo"

    assert_text "Consumo was successfully created"
    click_on "Back"
  end

  test "updating a Consumo" do
    visit consumos_url
    click_on "Edit", match: :first

    fill_in "Data Abastecimento", with: @consumo.data_abastecimento
    fill_in "Quantidade Litros", with: @consumo.quantidade_litros
    fill_in "Valor", with: @consumo.valor
    fill_in "Veiculo", with: @consumo.veiculo_id
    click_on "Update Consumo"

    assert_text "Consumo was successfully updated"
    click_on "Back"
  end

  test "destroying a Consumo" do
    visit consumos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Consumo was successfully destroyed"
  end
end
